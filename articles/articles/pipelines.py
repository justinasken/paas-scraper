# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

from multiprocessing import connection
import psycopg2
# useful for handling different item types with a single interface
from itemadapter import ItemAdapter

from .items import ArticlesItem
from scrapy.utils.project import get_project_settings

settings = get_project_settings()


class PostgresDBPipeline(object):

    def open_spider(self, spider):
        self.connection = psycopg2.connect(
            host = settings["POSTGRESDB_HOST"],
            user = settings["POSTGRESDB_USERNAME"],
            password = settings["POSTGRESDB_PASSWORD"],
            dbname = settings["POSTGRESDB_DATABASE"])
        self.cur = self.connection.cursor()

    def close_spider(self, spider):
        self.cur.close()
        self.connection.close()

    def process_item(self, item, spider):
        if isinstance(item, ArticlesItem):
            self.cur.execute('SELECT id, url, tags FROM article WHERE url = %s LIMIT 1', (item["url"],))
            row = self.cur.fetchone()
            if row is None:
                self.cur.execute('INSERT INTO article(title, url, date, tags, content) VALUES (%s,%s,%s,%s,%s)',(item["title"],item["url"],item['date'],item['tag'],item["content"]))
            else:
                if item["tag"][0] in row[2]:
                    spider.tags_skip[item['tag'][0]]=True
                else:
                    tags = row[2]
                    tags.append(item["tag"][0])
                    self.cur.execute('UPDATE article SET tags = %s where id = %s',(tags, row[0]))
        self.connection.commit()
        return item
