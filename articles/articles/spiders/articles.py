from email import header
from urllib import request
import scrapy
import numpy as np
from scrapy.selector import Selector
from scrapy.utils.project import get_project_settings
from ..items import ArticlesItem
import time
import re
import itertools
import datetime
settings = get_project_settings()


class ArticlesSpider(scrapy.Spider):
    name = "articles"
    AUTOTHROTTLE_ENABLED = True
    allowed_domains = [settings["ALLOWED_DOMAIN"]]
    tags = settings["TAG_LIST"]
    tags_skip = {key:False for key in tags}
    dynamic_wait_amount = 3
    max_articles_amount = 2000
    html = {}

    def start_requests(self):
        for tag in self.tags:
            url = settings["FIRST_DOMAIN"]+"0&q="+tag
            print("Started "+tag+"...\t\t\tArticles: 0-50")
            yield scrapy.Request(url = url, callback = self.parse, dont_filter=True, meta={
                'splash': {
                    'endpoint':'render.html',
                    'args':{'wait':self.dynamic_wait_amount}
                },
                "tag":tag,
                "from":0})

    def process_text(self, text):
        text = text.lower()
        text = " ".join(text.split())
        return text

    def monthToNum(self, shortMonth):
        return {
            'Jan': 1,
            'Feb': 2,
            'Mar': 3,
            'Apr': 4,
            'May': 5,
            'Jun': 6,
            'Jul': 7,
            'Aug': 8,
            'Sep': 9, 
            'Oct': 10,
            'Nov': 11,
            'Dec': 12
    }[shortMonth]

    def parse(self, response):
        results = response.xpath("//div[@class='cnn-search__result cnn-search__result--article']/div[@class='cnn-search__result-contents']")
        current_tag = response.meta.get("tag")
        from_tag = response.meta.get("from")
        if(len(results) <= 0):
            self.tags_skip[current_tag]=True
        else:
            for result in results:
                date = result.xpath("div[@class='cnn-search__result-publish-date']/span[not(@class)]/text()").get()
                date = date.split(',')
                md = date[0].split()
                m = md[0]
                day = md[1]
                y = date[1].strip()
                d = datetime.date(int(y), self.monthToNum(m), int(day))
                article_object = ArticlesItem(
                    url = result.xpath("h3[@class='cnn-search__result-headline']/a/@href").get()[2:],
                    title = result.xpath("h3[@class='cnn-search__result-headline']/a/text()").get(),
                    tag = [current_tag],
                    date = d.isoformat(),
                    content = self.process_text(result.xpath("div[@class='cnn-search__result-body']/text()").get())
                )
                yield article_object
            print("Finished "+current_tag+"...\t\t\tArticles: "+str(from_tag)+"-"+str(from_tag+50))

            if(self.tags_skip[current_tag]!=True and from_tag<self.max_articles_amount):
                from_tag_new = from_tag+50
                url_new = settings["FIRST_DOMAIN"]+str(from_tag_new)+"&q="+current_tag
                print("Started "+current_tag+"...\t\t\tArticles: "+str(from_tag_new)+"-"+str(from_tag_new+50))
                yield scrapy.Request(url = url_new, callback = self.parse, dont_filter=True, meta={
                    'splash': {
                        'endpoint':'render.html',
                        'args':{'wait':self.dynamic_wait_amount}
                    },
                    "tag":current_tag,
                    "from":from_tag_new})
        

